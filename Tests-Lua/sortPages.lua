
local pages0 = { 12, 1, 134, 23, 45, 177, 33, 56, 102,103,"9--31",104,105,106,107,108,109,110,111,112}


local pages2 = {"IX-162", "IX-163", "IX-164", "IX-165", "IX-166", "IX-167", "IX-168", "IX-171", "IX-173", "IX-187", "IX-191", "IX-192", "IX-2", "IX-200", "IX-224", "IX-234", "IX-235", "IX-236", "IX-237", "IX-238", "IX-240", "IX-241", "IX-243", "IX-251", "IX-253", "IX-255", "IX-256", "IX-257", "IX-258", "IX-259", "IX-260", "IX-261", "IX-266", "IX-268", "IX-269", "IX-286", "IX-294"}



pageNoPrefixDel = ""
if pageNoPrefixDel ~= "" then 
--  numericPage = false 
   pageNoPrefixPattern = "^.*"..pageNoPrefixDel
end

function pageCompare(a,b)  
-- a, b are something like "3" or "VI-17" or "9--31"
  if pageNoPrefixDel ~= "" then
    A = (a:gsub(pageNoPrefixPattern,''))
    B = (b:gsub(pageNoPrefixPattern,''))
    a = tonumber(A)
    b = tonumber(B)
  else
    A = tostring(a)
    B = tostring(b)
    A1 = A:find("--",1,true)
    B1 = B:find("--",1,true)
    if A1 then a = tonumber(A:sub(1,A1-1)) end
    if B1 then b = tonumber(B:sub(1,B1-1)) end
  end
print(a,b)
  return a < b
end

local minCompress = 2

pages1= {1, 2, 3, 9, 12, 13, 23, 24, 25, 26, "28--31", 33, 45, 56, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 134, 175, 176, 177}

function compressPageList(pages)
  local newPages = {}
  local series = {pages[1]}
  local checkPage = true
  for i=2,#pages do
    if tonumber(pages[i]) and checkPage then
      if (tonumber(pages[i])-tonumber(pages[i-1])) == 1 then
        series[#series+1] = pages[i]--         page difference is 1, add page to series
      elseif (#series == 1) then    --         only one page -> output
        newPages[#newPages+1] = series[1]
        series = {pages[i]}
      elseif (#series > minCompress) then  -- we found a page series
        newPages[#newPages+1] = series[1].."--"..series[#series]-- first..last
        series = {pages[i]}
      else -- series < minCompress
        for i=1,#series do
          newPages[#newPages+1] = series[i]
        end
        series = {pages[i]}
      end
    else -- current page is not a number, we simply add it
      checkPage = false
      if not tonumber(pages[i-1]) then 
        checkPage = true
        series = {pages[i]}
      else
        if (#series > minCompress) then  -- we found a page series
          newPages[#newPages+1] = series[1].."--"..series[#series]-- first..last
          series = {pages[i]}
        else -- series < minCompress
          for i=1,#series do
            newPages[#newPages+1] = series[i]
          end
          series = {pages[i]}
        end
      end  
    end
  end
  if (#series > (minCompress)) and ((tonumber(pages[#pages])-tonumber(pages[#pages-1])) == 1) then  -- last page
    newPages[#newPages+1] = series[1].."--"..pages[#pages]-- first..last
  else -- series < minCompress
    for i=1,#series-1 do
      newPages[#newPages+1] = series[i]
    end
    newPages[#newPages+1] = pages[#pages]
  end
  return newPages
end

--pages1 = { 
--  1--3, 9, 12, 23, 33, 45, 56, 134, 177, 102--112, 134, 177}

for i,v in ipairs(pages2) do 
  if v then print(v) end 
end

table.sort(pages2,pageCompare)

for i,v in ipairs(pages2) do 
  if v then print(v) end 
end

--[[
table.sort(pages1,pageCompare)

for i,v in ipairs(pages1) do 
  if v then print(v) end 
end

]]

local Pages = compressPageList(pages1)
for i,v in ipairs(Pages) do 
  if v then print(v) end 
end



