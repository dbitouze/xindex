--
function getCharType(c)
  local i = string.byte(c) -- works only for 1 byte chars
  if (i > 48) and (i < 57) then return 1 end
  if (i > 97) and (i < 122) or (c:byte()  >= 80 ) then return 2 end
  return 0
end

--function getCharType(c)
--   return #c:rep(3):match(".%w?%a?")-1
--end
print (getCharType("ö"))
print (getCharType("1"))
print (getCharType("!"))
print (getCharType("o"))

c = "Ä"
print ("Ä="..c:byte())
c = "Ö"
print ("Ö="..c:byte())
c = utf8(0xDC)
print ("Ü="..c)
