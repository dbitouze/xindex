--require ("unicode")
--require ("utf")

local category_data = dofile("unicode-category.txt")
local floor = math.floor
local function binary_range_search(code_point, ranges)
    local low, mid, high
    low, high = 1, #ranges
    while low <= high do
        mid = floor((low + high) / 2)
        local range = ranges[mid]
        if code_point < range[1] then
            high = mid - 1
        elseif code_point <= range[2] then
            return range, mid
        else
            low = mid + 1
        end
    end
    return nil, mid
end

function get_category(code_point)
    if category_data.singles[code_point] then
        return category_data.singles[code_point]
    else
        local range = binary_range_search(code_point, category_data.ranges)
        return range and range[3] or "Cn"
    end
end


function category_to_number(category) 
  if category == "Nd" then return 1 
  elseif category:sub(1, 1) == "L" then return 0
  else return 2
  end 
end


print(get_category(utf8.codepoint('ö')))
print(category_to_number(get_category(utf8.codepoint('ö'))))

print(get_category(utf8.codepoint('Ö')))
print(category_to_number(get_category(utf8.codepoint('Ö'))))

print(get_category(utf8.codepoint('^')))

print(category_to_number(get_category(utf8.codepoint('^'))))
