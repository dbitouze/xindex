str="\\idxtextClasses !{\\sffamily IEEEtran}"
str2="Beamer-Template!navigation symbols@\\texttt {navigation symbols}"
function getStrPart(str,n) -- return a substring
  if n>=0 then
    return (
      str:gsub("!.*", "%0!")
         :gsub("[^!]*!", "", n)
         :match("^([^!]*)!")
    )
  end
end

--select(2,string.gsub(v["Entry"],"!","!")) -- Number of !

print("Number of !: "..select(2,string.gsub(str,"!","!")))
print(getStrPart(str,0))
print(getStrPart(str,1))

print(getStrPart(str2,0))
print(getStrPart(str2,1))
