function replaceVerticalChar(v)
--  res, _ = string.gsub(v:gsub('|[()]$', ''), '|[()]?', ' \\')
--  return res
  if not v:match('|') then 
    return v,""
  else 
    return v:gsub('|.*',''), v:match('|.*'):gsub('|','\\')--:gsub("%s+", "")  -- part before, part after | without spaces
  end
end



print(replaceVerticalChar("abcd|foo"))
print(replaceVerticalChar("abcd|textsf{foo bar}"))
