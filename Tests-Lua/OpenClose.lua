function repl(v)
    res, _ = string.gsub(v:gsub('\\[()]%s*$', ''), '\\[()]%s*', ' \\')
    return res
end


print(repl("\\)"))
print(repl("\\("))
print(repl("\\)   "))
print(repl("\\(   "))
print(repl("\\)foo"))
print(repl("\\(foo"))
print(repl("\\)   foo"))
print(repl("\\(   foo"))
